import { Navigation } from 'react-native-navigation';
import { registerScreens } from './screens';


global.XMLHttpRequest = global.originalXMLHttpRequest ?
  global.originalXMLHttpRequest :
  global.XMLHttpRequest;
global.FormData = global.originalFormData ?
  global.originalFormData :
  global.FormData;

fetch; // Ensure to get the lazy property

// RNDebugger only
if (window.__FETCH_SUPPORT__) {
  window.__FETCH_SUPPORT__.blob = false
}


//make the register screens available
registerScreens();

//start a screen
Navigation.startSingleScreenApp({
  screen: {
    screen: 'Borrow.Auth',
    title: 'Auth'
  }
});