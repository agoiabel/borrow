import { Navigation } from 'react-native-navigation';

// import provider 
import { Provider } from 'react-redux';
import store from '../store';


import Auth from './Auth';
import Name from './Name';
import Address from './Address';
import Account from './Account';
import Loan from './Loan';

import Guarantor from './Guarantor';
import Employment from './Employment';

import Identity from './Identity';
import Term from './Term';
import SubmitLoan from './SubmitLoan';
import Dashboard from './Dashboard';

//Register screen
export function registerScreens() {
    Navigation.registerComponent('Borrow.Auth', () => Auth, store, Provider);
    Navigation.registerComponent('Borrow.Name', () => Name, store, Provider);
    Navigation.registerComponent('Borrow.Address', () => Address, store, Provider);
    Navigation.registerComponent('Borrow.Account', () => Account, store, Provider);
    
    Navigation.registerComponent('Borrow.Loan', () => Loan, store, Provider);
    Navigation.registerComponent('Borrow.Guarantor', () => Guarantor, store, Provider);
    Navigation.registerComponent('Borrow.Employment', () => Employment, store, Provider);

    Navigation.registerComponent('Borrow.Identity', () => Identity, store, Provider);
    Navigation.registerComponent('Borrow.Term', () => Term, store, Provider);
    Navigation.registerComponent('Borrow.SubmitLoan', () => SubmitLoan, store, Provider);
    Navigation.registerComponent('Borrow.Dashboard', () => Dashboard, store, Provider);
}