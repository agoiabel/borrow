import React, { Component } from 'react';
import {
	View,
	Text,
	Alert
} from 'react-native';

import moment from 'moment';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions';

import styles from './styles';
import Button from '../../components/UI/Button';
import CustomHeaderText from '../../components/UI/CustomHeaderText';
import TransactionContainer from '../../components/TransactionContainer';

class Dashboard extends Component {

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}

	componentDidMount () {}

	componentWillReceiveProps(nextProps) {

		if (nextProps.pay_loan_status !== '' || nextProps.pay_loan_status.length ) {

			if (nextProps.pay_loan_status === 200) {
				return Alert.alert(nextProps.pay_loan_message);
			} else {
				return Alert.alert('Oops', nextProps.pay_loan_message);
			}

		}
	}

	payNow = () => {
		this.props.payNow(this.props.loan_id);
	}

	render () {
		return (
			<View>
				<View style={styles.header}>
					<CustomHeaderText fontSize='16' color='#9C9C9C'> Dashboard </CustomHeaderText>
				</View>

				<View style={styles.dashboardContainer}>

					<View style={styles.loanContainer} elevation={5}>
						
						<View style={styles.dueDateTotalDue}>
							<View style={styles.dueDate}>
								<Text style={styles.dueDateTotalDueHeader}>DUE DATE</Text>
								<Text style={styles.dueDateTotalDueText}>{moment(this.props.loan_due_date).format('LL')}</Text>
							</View>
							<View style={styles.totalDue}>
								<Text style={styles.dueDateTotalDueHeader}>TOTAL DUE</Text>
								<Text style={styles.dueDateTotalDueText}>&#8358; {this.props.loan_total_due.toLocaleString()}</Text>
							</View>
						</View>

						<View style={styles.currentDue}>
							<Text style={styles.dueDateTotalDueHeader}>CURRENT DUE</Text>
							<CustomHeaderText fontSize='28' color='#787878'>&#8358; {this.props.loan_current_due.toLocaleString()}</CustomHeaderText>
						</View>
					</View>
					<View>
						<Button onPress={this.payNow}>Pay now</Button>
					</View>

					<TransactionContainer />


				</View>

			</View>
		);
	}

}

const mapStateToProps = state => {
	return {
		loan_current_due: state.authReducer.loan_current_due,
		loan_due_date: state.authReducer.loan_due_date,
		loan_total_due: state.authReducer.loan_total_due,
		loan_id: state.authReducer.loan_id,

		pay_loan_status: state.userReducer.pay_loan_status,
		pay_loan_message: state.userReducer.pay_loan_message
	}
}

const mapDispatchToProps = dispatch => {
	return {
		payNow: (loan_id) => dispatch(actionCreators.payNow(loan_id)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);