import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	header: {
		paddingTop: 20,
		paddingBottom: 15,
		alignItems: 'center',
	},
	dashboardContainer: {
		marginLeft: 'auto',
		marginRight: 'auto',
		width: '90%',
	},
	loanContainer: {
		maxWidth: '100%',

		backgroundColor: '#ffffff',
		shadowColor: '#000000',
		shadowOffset: {
			width: 0,
			height: 3
		},
		shadowRadius: 5,
		shadowOpacity: 0.10,

		flexDirection: 'column',

		paddingTop: 15,
		paddingLeft: 15,
		paddingRight: 15,
		paddingBottom: 15,

		marginBottom: 25,
	},
	dueDateTotalDue: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 12
	},
	currentDue: {
		alignItems: 'center',
	},
	dueDateTotalDueHeader: {
		color: '#CDCDCD'
	},
	dueDateTotalDueText: {
		color: '#868686'
	}
});

export default styles;