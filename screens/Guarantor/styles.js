import {
	StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
	container: {
		flexGrow: 1,
		paddingBottom: 10
	},
	headerContainer: {},
	formContainer: {
		marginTop: 15,
		flex: 1
	},
	questionContainer: {
		marginLeft: 'auto',
		marginRight: 'auto',
		width: '90%',
		flex: 1,
	},
	questionFormContainer: {
		marginBottom: 'auto'
	},
	buttonContainer: {}
});

export default styles;