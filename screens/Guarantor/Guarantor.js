import React, { Component } from 'react';
import {
	View,
	ScrollView,
	KeyboardAvoidingView
} from 'react-native';

import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions';

import styles from './styles';
import Header from '../../components/Header';
import validate from '../../utility/validation';
import Button from '../../components/UI/Button';
import CustomInput from '../../components/UI/CustomInput';

class Guarantor extends Component {

	constructor(props) {
		super(props);

		this.state = {
			formIsValid: (props.guarantor_name && props.guarantor_relationship && props.guarantor_phone_number) ? true : false,
			controls: {
				guarantor_name: {
					value: (props.guarantor_name) ? props.guarantor_name :  '',
					valid: (props.guarantor_name) ? true : false,
					touched: (props.guarantor_name) ? true : false,
					validationRules: {
						isRequired: true
					},
					placeholderText: (props.guarantor_name) ? props.guarantor_name :  'Guarantor name'
				},
				guarantor_relationship: {
					value: (props.guarantor_relationship) ? props.guarantor_relationship : '',
					valid: (props.guarantor_relationship) ? true : false,
					touched: (props.guarantor_relationship) ? true : false,
					validationRules: {
						isRequired: true
					},
					placeholderText: (props.guarantor_relationship) ? props.guarantor_relationship : 'Relationship with guarantor'
				},
				guarantor_phone_number: {
					value: (props.guarantor_phone_number) ? props.guarantor_phone_number : '',
					valid: (props.guarantor_phone_number) ? true : false,
					touched: (props.guarantor_phone_number) ? true : false,
					validationRules: {
						isRequired: true,
						minLength: 11,
						maxLength: 11						
					},
					placeholderText: (props.guarantor_phone_number) ? props.guarantor_phone_number : 'Guarantor phone number'
				}
			}
		}

	}

	navigateToNextPage = () => {
		const formData = {};
		for (let formElementId in this.state.controls) {
			formData[formElementId] = this.state.controls[formElementId].value
		}

		this.props.storeGuarantor(formData);

		this.props.navigator.push({
			screen: 'Borrow.Employment',
			title: 'Employment',
			passProps: {
				company_name: this.props.company_name,
				employer_name: this.props.employer_name,
				employer_phone_number: this.props.employer_phone_number,
				job_title: this.props.job_title
			}
		});
	}

	goBack = () => {
		this.props.navigator.push({
			screen: 'Borrow.Loan',
			title: 'Loan',
			passProps: {
				loan_amount: this.props.loan_amount,
				loan_purpose: this.props.loan_purpose,
				refund_date: this.props.refund_date,
				monthly_salary: this.props.monthly_salary
			}
		});
	}

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}

	updateInputChange = (key, value, placeholderText) => {
		const updatedControls = {
			...this.state.controls
		};
		const updatedFormElement = {
			...updatedControls[key]
		};
		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.placeholderText = placeholderText;
		updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

		updatedControls[key] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedControls) {
			formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			formIsValid: formIsValid,
			controls: updatedControls
		});
	}

	render() {
		return (

			<KeyboardAvoidingView style={styles.container}>
				<ScrollView contentContainerStyle={styles.container} showsVerticalScrollIndicator={false}>
					<View style={styles.headerContainer}>
						<Header questionTitle='Your Guarantor'
							questionSubtitle='Tell us about your guarantor'
							stepNumber='5'
							progress='55.55%'
							onPress={this.goBack}
							showBackButton={true}
						/>
					</View>
					<View style={styles.formContainer}>
						<View style={styles.questionContainer}>


							<View style={styles.questionFormContainer}>
								<View>
									<CustomInput placeholder={this.state.controls.guarantor_name.placeholderText}
										value={this.state.controls.guarantor_name.value}
										onChangeText={(value) => this.updateInputChange('guarantor_name', value, '')}
										valid={this.state.controls.guarantor_name.valid}
										touched={this.state.controls.guarantor_name.touched}
									/>
								</View>

								<View>
									<CustomInput placeholder={this.state.controls.guarantor_relationship.placeholderText}
										value={this.state.controls.guarantor_relationship.value}
										onChangeText={(value) => this.updateInputChange('guarantor_relationship', value, '')}
										valid={this.state.controls.guarantor_relationship.valid}
										touched={this.state.controls.guarantor_relationship.touched}
									/>
								</View>

								<View>
									<CustomInput placeholder={this.state.controls.guarantor_phone_number.placeholderText}
										value={this.state.controls.guarantor_phone_number.value}
										onChangeText={(value) => this.updateInputChange('guarantor_phone_number', value, '')}
										valid={this.state.controls.guarantor_phone_number.valid}
										touched={this.state.controls.guarantor_phone_number.touched}
										keyboardType='numeric'
									/>
								</View>

								{/* <View>
									<CustomInput placeholder='Employer phone number'
										value={this.state.controls.employer_phone_number.value}
										onChangeText={(value) => this.updateInputChange('employer_phone_number', value, '')}
										valid={this.state.controls.employer_phone_number.valid}
										touched={this.state.controls.employer_phone_number.touched}
										keyboardType='numeric'
									/>
								</View> */}

							</View>

							<View style={styles.buttonContainer}>
								<Button
									onPress={this.navigateToNextPage}
									disabled={!this.state.formIsValid}
									>
										Continue
                                </Button>
							</View>


						</View>
					</View>
				</ScrollView>
			</KeyboardAvoidingView>

		);
	}

}


const mapStateToProps = state => {
	return {
		loan_amount: state.userReducer.loan_amount,
		loan_purpose: state.userReducer.loan_purpose,
		refund_date: state.userReducer.refund_date,
		monthly_salary: state.userReducer.monthly_salary,

		company_name: state.userReducer.company_name,
		employer_name: state.userReducer.employer_name,
		employer_phone_number: state.userReducer.employer_phone_number,
		job_title: state.userReducer.job_title
	}
}

const mapDispatchToProps = dispatch => {
	return {
		storeGuarantor: (formData) => dispatch(actionCreators.storeGuarantor(formData))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Guarantor);