import React, { Component } from 'react';
import {
	View,
	Image,
	Platform,
} from 'react-native';

import styles from './styles';
import Header from '../../components/Header';
import Button from '../../components/UI/Button';

import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions';

import RNImagePicker from 'react-native-image-picker';
import ImagePickerAndroidWrapper from './AndroidWrapperForImagePicker';

class Identity extends Component {

    constructor(props) {
        super(props);

        this.state = {
			pickedImage: null,
			showLoading: false
        };
    }

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}

	componentWillReceiveProps (nextProps) {
		if (nextProps.image_path || nextProps.image_path.length) {
			this.setState({
				showLoading: false
			});			
		}
	}

	storeImageData = image => {
		this.setState({
			showLoading: true
		});

		formData = {
			image: image.data
		}

		this.props.storeImage(formData);
	}

    navigateToNextPage = () => {
		this.props.navigator.push({
		  screen: 'Borrow.Term',
		  title: 'Term'
		});
    }

	goBack = () => {
		this.props.navigator.push({
		  screen: 'Borrow.Employment',
		  title: 'Employment',
		  passProps: {
			  company_name: this.props.company_name,
			  employer_name: this.props.employer_name,
			  employer_phone_number: this.props.employer_phone_number,
			  job_title: this.props.job_title
		  }
		});
	}

	takeAPhotoHandler = () => {

		const ImagePicker = Platform.OS === 'ios' ? RNImagePicker : ImagePickerAndroidWrapper;

		const Options = {
			title: 'Pick an image'
		};


		ImagePicker.showImagePicker(Options, response => {

			if (response.didCancel) {
			} else if (response.error) {
			} else {
				this.setState({
					pickedImage: { uri: response.uri },
				});

				this.storeImageData(response);
			}

		});

	}

	render () {

		return (
			<View style={styles.IdentityContainer}>

				<View style={styles.headerContainer}>
					<Header questionTitle='Your picture' 
							questionSubtitle='Snap yourself while holding your ID Card' 
							stepNumber='7' 
							progress='77.77%' 
							onPress={this.goBack}
							showBackButton={true}
					/>
				</View>

				<View style={styles.bodyContainer}>
					<View style={styles.questionContainer}>
						<Button onPress={this.takeAPhotoHandler} disabled={this.state.showLoading === true} isLoading={this.state.showLoading}> 
							Click to take a picture 
						</Button>
						
						<View style={styles.previewImageContainer}>
							<Image source={this.state.pickedImage} style={styles.previewImage} /> 
						</View>
					</View>
					<View style={styles.footerContainer}>
						<Button onPress={this.navigateToNextPage}
								disabled={this.state.pickedImage === null}
							>
							 Continue 
						</Button>
					</View>
				</View>
			</View>
		);
	}

}

const mapStateToProps = state => {
	return {
		image_path: state.userReducer.image_path,

		company_name: state.userReducer.company_name,		
		employer_name: state.userReducer.employer_name,		
		employer_phone_number: state.userReducer.employer_phone_number,		
		job_title: state.userReducer.job_title
	}
}

const mapDispatchToProps = dispatch => {
	return {
		storeImage: (formData) => dispatch(actionCreators.storeImage(formData))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Identity);