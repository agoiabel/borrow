import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	IdentityContainer: {
		flex: 1,
		marginBottom: 15
	},
	bodyContainer: {
		marginLeft: 'auto',
		marginRight: 'auto',
		width: '90%',

		flex: 1
	},
	questionContainer: {
		flex: 1,
		width: '100%',
		marginTop: 25
	},
	previewImageContainer: {
		marginTop: 25,
		marginBottom: 25,
	},
	previewImage: {
		width: '100%',
		height: '80%'
	}
});

export default styles;