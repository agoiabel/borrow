import React, { Component } from 'react';
import {
    View,
} from 'react-native';

import Button from '../../../components/UI/Button';
import CustomInput from '../../../components/UI/CustomInput';
import CustomPicker from '../../../components/UI/CustomPicker';

import validate from '../../../utility/validation';
import styles from './styles';


class NameForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            formIsValid: (props.firstname && props.lastname && props.gender) ? true : false,
            controls: {
                firstname: {
                    value: (props.firstname) ? props.firstname : '',
                    valid: (props.firstname) ? true : false,
                    validationRules: {
                        isRequired: true
                    },
                    placeholderText: (props.firstname) ? props.firstname : 'Firstname',
                    touched: (props.firstname) ? true : false
                },
                lastname: {
                    value: (props.lastname) ? props.lastname : '',
                    valid: (props.lastname) ? true : false,
                    validationRules: {
                        isRequired: true
                    },
                    placeholderText: (props.lastname) ? props.lastname : 'Lastname',
                    touched: (props.lastname) ? true : false
                },
                gender: {
                    value: (props.gender) ? props.gender : '',
                    valid: (props.gender) ? true : false,
                    touched: (props.lastname) ? true : false,
                    validationRules: {
                        isRequired: true
                    },
                    placeholderText: (props.gender) ? props.gender : 'Choose your gender',
                    options: [
                        {
                            label: 'Male',
                            value: 'Male',
                        },
                        {
                            label: 'Female',
                            value: 'Female',
                        }
                    ]
                }
            },
        };

        
    }

    selectFormHandler = option => {
        this.updateInputChange('gender', option.value, option.label);
    }


    updateInputChange = (key, value, placeholderText) => {
        const updatedControls = {
            ...this.state.controls
        };
        const updatedFormElement = {
            ...updatedControls[key]
        };
        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.placeholderText = placeholderText;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        updatedControls[key] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }

        this.setState({
            formIsValid: formIsValid,
            controls: updatedControls
        });
    }


	/**
	 * Handle the login process
	 * 
	 * @return 
	 */
    submitFormAndNavigate = () => {
        const formData = {};
        for (let formElementId in this.state.controls) {
            formData[formElementId] = this.state.controls[formElementId].value
        }

        this.props.navigateToNextPage(formData);
    }


    render() {

        return (
            <View style={styles.questionContainer}>


                <View style={styles.questionFormContainer}>
                    <View>
                        <CustomInput placeholder='Firstname'
                            value={this.state.controls.firstname.value}
                            onChangeText={(value) => this.updateInputChange('firstname', value, '')}
                            valid={this.state.controls.firstname.valid}
                            touched={this.state.controls.firstname.touched}
                        />
                    </View>
                    <View>
                        <CustomInput placeholder='Lastname'
                            value={this.state.controls.lastname.value}
                            onChangeText={(value) => this.updateInputChange('lastname', value, '')}
                            valid={this.state.controls.lastname.valid}
                            touched={this.state.controls.lastname.touched}
                        />
                    </View>
                    <View>
                        <CustomPicker options={this.state.controls.gender.options}
                            selectedValue={this.state.controls.gender.placeholderText}
                            onValueChange={option => this.selectFormHandler(option)}
                            valid={this.state.controls.gender.valid}
                            touched={this.state.controls.gender.touched}
                        />
                    </View>

                    
                </View>

                <View style={styles.buttonContainer}>
                    <Button
                        onPress={this.submitFormAndNavigate}
                        disabled={!this.state.formIsValid}
                        >
                        Continue
                    </Button>
                </View>


            </View>
        )

    }
}

export default NameForm;