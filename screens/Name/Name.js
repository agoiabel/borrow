import React, { Component } from 'react';
import {
	View,
	ScrollView,
	KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';

import Header from '../../components/Header';
import NameForm from './NameForm';
import styles from './styles';

import * as actionCreators from '../../store/actions';

class Name extends Component {

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}


	navigateToNextPage = formData => {

		this.props.storeName(formData);

		return this.props.navigator.push({
			screen: 'Borrow.Address',
			title: 'Address',
			passProps: {
				address: this.props.address,
				city: this.props.city,
				state: this.props.state
			}
		});

	}


	render() {
		return (
			<KeyboardAvoidingView style={styles.container}>
				<ScrollView contentContainerStyle={styles.container} showsVerticalScrollIndicator={false}>
					<View style={styles.headerContainer}>
						<Header questionTitle='Your Name'
							questionSubtitle='We need your primary details'
							stepNumber='1'
							progress='11.11%'
							onPress={this.goBack}
							showBackButton={false}
						/>
					</View>
					<View style={styles.formContainer}>
						<NameForm 
							navigateToNextPage={(formData) => this.navigateToNextPage(formData)} 
							firstname={this.props.firstname}	
							lastname={this.props.lastname}	
							gender={this.props.gender}	
						/>
					</View>

				</ScrollView>
			</KeyboardAvoidingView>
		);

	}

}

const mapStateToProps = state => {
	return {
		address: state.userReducer.address,
		city: state.userReducer.city,
		state: state.userReducer.state
	}
}

const mapDispatchToProps = dispatch => {
	return {
		storeName: (formData) => dispatch(actionCreators.storeName(formData))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Name);