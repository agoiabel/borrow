import React, { Component } from 'react';
import {
	View,
	ScrollView,
	KeyboardAvoidingView
} from 'react-native';

import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions';

import styles from './styles';
import Header from '../../components/Header';
import validate from '../../utility/validation';
import Button from '../../components/UI/Button';
import CustomInput from '../../components/UI/CustomInput';

class Employment extends Component {

	constructor(props) {
		super(props);

		this.state = {
			formIsValid: (props.company_name && props.employer_name && props.employer_phone_number && props.job_title) ? true : false,
			controls: {
				company_name: {
					value: (props.company_name) ? props.company_name : '',
					valid: (props.company_name) ? true : false,
					touched: (props.company_name) ? true : false,
					validationRules: {
						isRequired: true
					},
					placeholderText: (props.company_name) ? props.company_name : 'Company name'
				},
				employer_name: {
					value: (props.employer_name) ? props.employer_name : '',
					valid: (props.employer_name) ? true : false,
					touched: (props.employer_name) ? true : false,
					validationRules: {
						isRequired: true
					},
					placeholderText: (props.employer_name) ? props.employer_name : 'Employer name'
				},
				employer_phone_number: {
					value: (props.employer_phone_number) ? props.employer_phone_number : '',
					valid: (props.employer_phone_number) ? true : false,
					touched: (props.employer_phone_number) ? true : false,
					validationRules: {
						isRequired: true,
						minLength: 11,
						maxLength: 11
					},
					placeholderText: (props.employer_phone_number) ? props.employer_phone_number : 'Employer phone number'
				},
				job_title: {
					value: (props.job_title) ? props.job_title : '',
					valid: (props.job_title) ? true : false,
					touched: (props.job_title) ? true : false,
					validationRules: {
						isRequired: true
					},
					placeholderText: (props.job_title) ? props.job_title : 'Job title'
				},
			}
		}

	}

	navigateToNextPage = () => {
		const formData = {};
		for (let formElementId in this.state.controls) {
			formData[formElementId] = this.state.controls[formElementId].value
		}

		this.props.storeEmployment(formData);

		this.props.navigator.push({
			screen: 'Borrow.Identity',
			title: 'Identity'
		});
	}

	goBack = () => {
		this.props.navigator.push({
			screen: 'Borrow.Guarantor',
			title: 'Guarantor',
			passProps: {
				guarantor_name: this.props.guarantor_name,
				guarantor_relationship: this.props.guarantor_relationship,
				guarantor_phone_number: this.props.guarantor_phone_number,
			}
		});
	}

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}

	updateInputChange = (key, value, placeholderText) => {
		const updatedControls = {
			...this.state.controls
		};
		const updatedFormElement = {
			...updatedControls[key]
		};
		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.placeholderText = placeholderText;
		updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

		updatedControls[key] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedControls) {
			formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			formIsValid: formIsValid,
			controls: updatedControls
		});
	}

	render() {
		return (

			<KeyboardAvoidingView style={styles.container}>
				<ScrollView contentContainerStyle={styles.container} showsVerticalScrollIndicator={false}>
					<View style={styles.headerContainer}>
						<Header questionTitle='Your Profession'
							questionSubtitle='Tell us about your profession'
							stepNumber='6'
							progress='66.66%'
							onPress={this.goBack}
							showBackButton={true}
						/>
					</View>
					<View style={styles.formContainer}>
						<View style={styles.questionContainer}>


							<View style={styles.questionFormContainer}>
								<View>
									<CustomInput placeholder={this.state.controls.company_name.placeholderText}
										value={this.state.controls.company_name.value}
										onChangeText={(value) => this.updateInputChange('company_name', value, '')}
										valid={this.state.controls.company_name.valid}
										touched={this.state.controls.company_name.touched}
									/>
								</View>

								<View>
									<CustomInput placeholder={this.state.controls.employer_name.placeholderText}
										value={this.state.controls.employer_name.value}
										onChangeText={(value) => this.updateInputChange('employer_name', value, '')}
										valid={this.state.controls.employer_name.valid}
										touched={this.state.controls.employer_name.touched}
									/>
								</View>

								<View>
									<CustomInput placeholder={this.state.controls.employer_phone_number.placeholderText}
										value={this.state.controls.employer_phone_number.value}
										onChangeText={(value) => this.updateInputChange('employer_phone_number', value, '')}
										valid={this.state.controls.employer_phone_number.valid}
										touched={this.state.controls.employer_phone_number.touched}
										keyboardType='numeric'
									/>
								</View>

								<View>
									<CustomInput placeholder={this.state.controls.job_title.placeholderText}
										value={this.state.controls.job_title.value}
										onChangeText={(value) => this.updateInputChange('job_title', value, '')}
										valid={this.state.controls.job_title.valid}
										touched={this.state.controls.job_title.touched}
									/>
								</View>
							</View>

							<View style={styles.buttonContainer}>
								<Button
									onPress={this.navigateToNextPage}
									disabled={!this.state.formIsValid}
									>
										Continue
                                </Button>
							</View>


						</View>
					</View>
				</ScrollView>
			</KeyboardAvoidingView>

		);
	}

}


const mapStateToProps = state => {
	return {
		guarantor_name: state.userReducer.guarantor_name,
		guarantor_relationship: state.userReducer.guarantor_relationship,
		guarantor_phone_number: state.userReducer.guarantor_phone_number
	}
}

const mapDispatchToProps = dispatch => {
	return {
		storeEmployment: (formData) => dispatch(actionCreators.storeEmployment(formData))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Employment);