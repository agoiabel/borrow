import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	TermContainer: {
		flex: 1,
		marginBottom: 15
	},
	bodyContainer: {
		marginTop: 25,
		marginLeft: 'auto',
		marginRight: 'auto',
		width: '90%',

		flex: 1
	},
	loanPrice: {
		maxWidth: '100%',
		paddingTop: 20,
		paddingBottom: 20,
		paddingRight: 15,
		paddingLeft: 15,
		backgroundColor: '#ffffff',
		shadowColor: '#000000',
		shadowOffset: {
			width: 0,
			height: 3
		},
		shadowRadius: 5,
		shadowOpacity: 0.10,

		justifyContent: 'space-between',
		flexDirection: 'row',
		marginTop: 15,
		marginBottom: 15
	},
	landscapeLoanPrice: {
		maxWidth: '100%',
		paddingTop: 5,
		paddingBottom: 5,
		paddingRight: 15,
		paddingLeft: 15,
		backgroundColor: '#ffffff',
		shadowColor: '#000000',
		shadowOffset: {
			width: 0,
			height: 3
		},
		shadowRadius: 5,
		shadowOpacity: 0.10,

		justifyContent: 'space-between',
		marginTop: 15,
		marginBottom: 15,

		flexDirection: 'column'
	},
	loanHeader: {
		color: '#9C9C9C',
		marginBottom: 10
	},
	loanBody: {
		color: '#9C9C9C'
	},
	loanAmount: {
		paddingTop: 5,
		paddingBottom: 5,

		alignItems: 'center'
	},
	totalRefund: {
		paddingTop: 5,
		paddingBottom: 5,

		alignItems: 'center'
	},
	refundDate: {
		paddingTop: 5,
		paddingBottom: 5,
		alignItems: 'center'
	},
	verticalLine: {
	    height: '100%',
		borderLeftWidth: 1,
		borderStyle: 'solid',
		borderLeftColor: '#9C9C9C',
	},
	
	portraitTermInstructionContainer: {
		flex: 1
	},
	landscapeTermInstructionContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between' 
	},
	portraitInstructionContainer: {
		paddingTop: 20,
		paddingBottom: 20,
		paddingRight: 15,
		paddingLeft: 15,
		backgroundColor: '#ffffff',
		shadowColor: '#000000',
		shadowOffset: {
			width: 0,
			height: 3
		},
		shadowRadius: 5,
		shadowOpacity: 0.10,	
	},
	landscapeInstructionContainer: {
		paddingTop: 5,
		paddingBottom: 5,
		paddingRight: 15,
		paddingLeft: 15,
		backgroundColor: '#ffffff',
		shadowColor: '#000000',
		shadowOffset: {
			width: 0,
			height: 3
		},
		shadowRadius: 5,
		shadowOpacity: 0.10,	

		width: '53%'
	},
	instructionsHeader: {
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 15
	},
	instruction: {
		marginBottom: 15,
		color: '#9C9C9C'
	},
	landscapeTermContainer: {
		width: '45%'	
	}
});

export default styles;