import React, { Component } from 'react';
import {
	View,
	Text,
	Alert,
	Platform,
	Dimensions,
	PermissionsAndroid
} from 'react-native';

import CallLogs from 'react-native-call-log';
import simpleContacts from 'react-native-simple-contacts';
const SmsAndroid = require('react-native-sms-android');

import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions';

import styles from './styles';
import Header from '../../components/Header';
import Button from '../../components/UI/Button';
import CustomHeaderText from '../../components/UI/CustomHeaderText';

class Term extends Component {

    constructor(props) {
        super(props);

        this.state = {
			viewMode: Dimensions.get('window').height > 500 ? 'portrait' : 'landscape',
			showLoading: false
        };
    }

	componentWillReceiveProps(nextProps) {
		if ((nextProps.contact.length !== 0 || nextProps.contact !== '') && (nextProps.sms.length !== 0 || nextProps.sms !== '') && (nextProps.call_log.length !== 0 || nextProps.call_log !== '')) {
			
			if (nextProps.contact !== nextProps.sms) {
				
				this.setState({
					showLoading: false
				});

				return this.props.navigator.push({
					screen: 'Borrow.SubmitLoan',
					title: 'SubmitLoan'
				});
			}
			
		}

		if (nextProps.can_apply_request.length !== 0 || nextProps.can_apply_request !== '') {

			return this.props.navigator.push({
				screen: 'Borrow.Auth',
				title: 'Auth'
			});
			
		}
	}


	getActualCallLogs = async () => {
		CallLogs.show(logs => {

			userCallLog = {
				call_log: logs
			};

			this.props.storeCallLog(userCallLog);
		});
	}

	getContacts = async () => {
		const contacts = await simpleContacts.getContacts();

		userContact = {
			contact: contacts
		};

		this.props.storeContact(userContact);
	}

	getActualSms = async () => {
		var filter = {
			box: '', // 'inbox' (default), 'sent', 'draft', 'outbox', 'failed', 'queued', and '' for all
		};
		SmsAndroid.list(JSON.stringify(filter), (fail) => {
			console.log("OH Snap: " + fail)
		},
			(count, smsList) => {
				userSms = {
					sms: smsList
				};
				this.props.storeSms(userSms);
			});
	}

	getCallLogs = async () => {
		if (Platform.Version > 23) {

			try {
				const granted = await PermissionsAndroid.request(
					PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
					{
						'title': 'Borrow Call Log Permission',
						'message': 'Borrow  needs access to your call logs so we can give loan.'
					}
				)

				if (granted === PermissionsAndroid.RESULTS.GRANTED) {

					this.getActualCallLogs();

				} else {
					Alert("Call log permission denied, can not continue processing the loan");
				}
			} catch (err) {
				console.warn(err)
			}

		} else {

			this.getActualCallLogs();

		}

	}


	getContactOnAndroid = async () => {
		if (Platform.Version > 23) {

			try {
				const granted = await PermissionsAndroid.request(
					PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
					{
						'title': 'Access your contact',
						'message': 'We need this to process your loan'
					}
				)
				if (granted === PermissionsAndroid.RESULTS.GRANTED) {
					this.getContacts();
				} else {
					Alert("Loan wont be process");
				}
			} catch (err) {
				Alert(err)
			}

		} else {
			this.getContacts();
		}
	}

	getSms = async () => {

		if (Platform.Version > 23) {
		    try {
		      const granted = await PermissionsAndroid.request(
		        PermissionsAndroid.PERMISSIONS.READ_SMS,
		        {
		          'title': 'Read Sms',
		          'message': 'Read sms '
		        }
		      )
		      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
		      	this.getActualSms();
		      } else {
		        Alert("Sms permission denied");
		      }
		    } catch (err) {
		      Alert(err);
		    }
		} else {
			this.getActualSms();
		}
	}


	acceptTerm = () => {
		
		this.setState({
			showLoading: true
		});

		if (Platform.OS === 'android') {
			this.getCallLogs(); 
			this.getContactOnAndroid();
			this.getSms();
		} else {
			this.getContacts(); //get only contacts on IOS
		}
	}

	/** Reapply for loan */
	reApply = () => {
		this.props.reApply(this.props.user.id);
	}

	goBack = () => {
		this.props.navigator.push({
		  screen: 'Borrow.Identity',
		  title: 'Identity'
		});
	}

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}

	render () {
		const DEFAULT = 0;
    	const AWAITING_LOAN = 1;
    	const LOAN_APPROVED = 2;
    	const LOAN_CONFIRMATION_COMPLETED = 3;
    	const LOAN_PAID_BACK = 5;
    	const LOAN_DECLINED = 6;

    	let button = (
    		<View></View>
    	);

		if (this.props.user.status == DEFAULT) {
			button = (
				<Button
					onPress={this.acceptTerm}
					isLoading={this.state.showLoading}
					disabled={this.state.showLoading}
						>
						Accept Terms
				</Button>
	    	);
		}

		let instructionContent = (
			<View style={styles.instructions}>
				<Text style={styles.instruction}>
					We will charge you #100 when you click the button below, we need this to transfer the loan to your account when you eligible 
				</Text>
			</View>
		);

		if (this.props.user.status == AWAITING_LOAN) {
			instructionContent = (
				<View style={styles.instructions}>
					<Text style={styles.instruction}>
						We are still validating your loan application, we will get intouch soon 
					</Text>
				</View>
			);
		}
		if (this.props.user.status == LOAN_APPROVED) {
			instructionContent = (
				<View style={styles.instructions}>
					<Text style={styles.instruction}>
						Loan approved, please check your email and make initial payment 
					</Text>
				</View>
			);
		}
		if (this.props.user.status == LOAN_CONFIRMATION_COMPLETED) {
			instructionContent = (
				<View style={styles.instructions}>
					<Text style={styles.instruction}>
						You will recieve money in your account shortly
					</Text>
				</View>
			);
		}
		if (this.props.user.status == LOAN_DECLINED) {
			instructionContent = (
				<View style={styles.instructions}>
					<Text style={styles.instruction}>
						Sorry, but your loan application was declined
					</Text>
					<Button
						onPress={this.reApply}
						isLoading={this.state.showLoading}
						>
						Re-apply
					</Button>
				</View>
			);
		}
		if (this.props.user.status == LOAN_PAID_BACK) {
			instructionContent = (
				<View style={styles.instructions}>
					<Text style={styles.instruction}>
						You have successfully paid your loan
					</Text>
					<Button
						onPress={this.reApply}
						isLoading={this.state.showLoading}
						>
						Re-apply
					</Button>
				</View>
			);
		}

		return (
			<View style={styles.TermContainer}>

				<View style={styles.headerContainer}>
					<Header questionTitle='Terms' 
							questionSubtitle='Please take your time to read our terms' 
							stepNumber='8' 
							progress='88.88%' 
							onPress={this.goBack}
							showBackButton={this.props.user.status === DEFAULT ? true : false}
					/>
				</View>

				<View style={styles.bodyContainer}>
					<View style={this.state.viewMode === 'portrait' ? styles.portraitInstructionContainer : styles.landscapeInstructionContainer} elevation={5}>
						<View style={styles.instructionsHeader}>  
							<CustomHeaderText fontSize='18' color='#9C9C9C'> Instructions </CustomHeaderText>
						</View>

						{ instructionContent }
						
						{ button }
					</View>
				</View>

			</View>
		);
	}

}

const mapStateToProps = state => {
	return {
		contact: state.userReducer.contact,
		call_log: state.userReducer.call_log,
		sms: state.userReducer.sms,

		user: state.authReducer.user,
		can_apply_request: state.userReducer.can_apply_request
	}
}

const mapDispatchToProps = dispatch => {
	return {
		storeContact: (userContact) => dispatch(actionCreators.storeContact(userContact)),
		storeCallLog: (userCallLog) => dispatch(actionCreators.storeCallLog(userCallLog)),
		storeSms: (userSms) => dispatch(actionCreators.storeSms(userSms)),
		reApply: (userId) => dispatch(actionCreators.reApply(userId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Term);