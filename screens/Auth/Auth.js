import React, { Component } from 'react';
import {
	View,
	Text,
	Image,
	Alert,
	ScrollView,
	KeyboardAvoidingView
} from 'react-native';

import { connect } from 'react-redux';


import styles from './styles';
import Button from '../../components/UI/Button';
import CustomInput from '../../components/UI/CustomInput';
import CustomHeaderText from '../../components/UI/CustomHeaderText';
import validate from '../../utility/validation';

import Logo from '../../assets/images/logo-medium.png';
import * as actionCreator from '../../store/actions';

class Auth extends Component {

	constructor () {
		super();
		this.state = {
			formIsValid: false,
			controls: {
				email: {
					value: '',
					valid: false,
					validationRules: {
						isEmail: true,
						isRequired: true
					},
					placeholderText: 'Email Address',
					touched: false
				},
				password: {
					value: '',
					valid: false,
					validationRules: {
						isRequired: true
					},
					placeholderText: 'Password',
					touched: false
				}
			},
			showLoading: false
		};
	}

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}

	componentWillReceiveProps (nextProps) {
		if (nextProps.auth_status === 200) {
			return this.redirectOnToken(nextProps.user.status);
		}

		if (nextProps.auth_status === 422) {
			this.setState({
				showLoading: false
			});
			return Alert.alert('Opps', nextProps.err_message); 
		}
	}

	redirectOnToken = status => {

		const LOAN_GIVEN = 4;

		if (status === 0) {
			return this.props.navigator.push({
				screen: 'Borrow.Name',
				title: 'Name'
			});
		}
		if (status === LOAN_GIVEN) {
			return this.props.navigator.push({
				screen: 'Borrow.Dashboard',
				title: 'Dashboard'
			});
		}
		return this.props.navigator.push({
			screen: 'Borrow.Term',
			title: 'Term'
		});
		
	}

	updateInputChange = (key, value, placeholderText) => {
		const updatedControls = {
			...this.state.controls
		};
		const updatedFormElement = {
			...updatedControls[key]
		};
		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.placeholderText = placeholderText;
		updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

		updatedControls[key] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedControls) {
			formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			formIsValid: formIsValid,
			controls: updatedControls
		});
	}

	/**
	 * Handle the login process
	 * 
	 * @return 
	 */
	loginHandler = async () => {
		this.setState({
			showLoading: true
		});
		const formData = {};
		for (let formElementId in this.state.controls) {
			formData[formElementId] = this.state.controls[formElementId].value
		}
		this.props.startLogin(formData);
	}

	render () {

		return (


			<KeyboardAvoidingView style={styles.container} behavior="padding" enabled>

				<ScrollView contentContainerStyle={styles.bodyContainer} showsVerticalScrollIndicator={false}>

					<View style={styles.topContainer}>
						<View style={styles.headerContainer}>
							<Image source={Logo} style={styles.logo} />
						</View>
						<View style={styles.bodyTitle}>
							<CustomHeaderText fontSize='25' color='#787878'> Borrow </CustomHeaderText>
							<CustomHeaderText fontSize='13' color='#787878'> Get a loan easily </CustomHeaderText>
						</View>
					</View>

					<View style={styles.footerContainer}>
						<CustomInput placeholder={this.state.controls.email.placeholderText}
							keyboardType="email-address"
							value={this.state.controls.email.value}
							valid={this.state.controls.email.valid}
							touched={this.state.controls.email.touched}
							onChangeText={(value) => this.updateInputChange('email', value, '')}
							autoCapitalize={'none'}
						/>
						<CustomInput placeholder={this.state.controls.password.placeholderText}
							keyboardType="default"
							secureTextEntry={true}
							value={this.state.controls.password.value}
							valid={this.state.controls.password.valid}
							touched={this.state.controls.password.touched}
							onChangeText={(value) => this.updateInputChange('password', value, '')}
						/>

						<Button onPress={this.loginHandler}
								disabled={!this.state.formIsValid}
								isLoading={this.state.showLoading}
							>
								Get Started
						</Button>
					</View>

				</ScrollView>
				

			</KeyboardAvoidingView>
		);
	}
}

const mapStateToProps = state => {
	return {
		auth_status: state.authReducer.auth_status,
		err_message: state.authReducer.err_message,
		user: state.authReducer.user,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		startLogin: (formData) => dispatch( actionCreator.startLogin(formData) ),
		// clearOldLogin: () => dispatch( actionCreator.clearOldLogin() )
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);