import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	container: {
		paddingTop: 70,	
		paddingBottom: 15,
		
		
		marginLeft: 'auto',
		marginRight: 'auto',
		width: '90%',
		flexGrow: 1
	},
	bodyContainer: {
		flexGrow: 1
	},
	topContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 'auto'
	},
	headerContainer: {
	},
	logo: {},
	bodyTitle: {},
	footerContainer: {
		marginBottom: 25
	},
	buttonContent: {
		flexDirection: 'row',
	},
	buttonContentText: {
		color: '#ffffff',
		marginLeft: 15
	}
});

export default styles;