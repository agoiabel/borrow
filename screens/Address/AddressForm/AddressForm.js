import React, { Component } from 'react';
import {
	View
} from 'react-native';

import validate from '../../../utility/validation';

import Button from '../../../components/UI/Button';
import CustomInput from '../../../components/UI/CustomInput';
import CustomPicker from '../../../components/UI/CustomPicker';

import styles from './styles';

class AddressForm extends Component {

    constructor(props) {
        super(props);

		let newStateArray = [];
		props.states.map(state => {
			newStateArray.push({
				label: state.name,
				value: state.id.toString()
			})
		});


        this.state = {
			formIsValid: (props.address && props.city && props.state) ? true : false,
            controls: {
            	address: {
					value: (props.address) ? props.address : '',
					valid: (props.address) ? true : false,
					touched: (props.address) ? true : false,
            		validationRules: {
            			isRequired: true
					},
					placeholderText: (props.address) ? props.address : 'Address' 
            	},
            	city: {
					value: (props.city) ? props.city : '',
					valid: (props.city) ? true : false,
					touched: (props.city) ? true : false,
            		validationRules: {
            			isRequired: true
					},
					placeholderText: (props.city) ? props.city : 'City'
            	},
            	state: {
					value: (props.state) ? props.state : 'Select your state',
					valid: (props.state) ? true : false,
					touched: (props.state) ? true : false,
            		validationRules: {
            			isRequired: true
            		},
					placeholderText: (props.state) ? this.getLabelForm(props.state, newStateArray) : 'Choose your state',
					options: newStateArray
            	}
            }
		};
    }


	getLabelForm = (value, array) => {

		let label = null;

		array.map(option => {
			if (option.value == value) {
				label = option.label;
			}
		});

		return label;
	}

	selectFormHandler = option => {
		this.updateInputChange('state', option.value, option.label);
	}


	updateInputChange = (key, value, placeholderText) => {
		const updatedControls = {
			...this.state.controls
		};
		const updatedFormElement = {
			...updatedControls[key]
		};
		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.placeholderText = placeholderText;
		updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

		updatedControls[key] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedControls) {
			formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			formIsValid: formIsValid,
			controls: updatedControls
		});
	}


	/**
	 * Handle the login process
	 * 
	 * @return 
	 */
	submitFormAndNavigate = () => {
		const formData = {};
		for (let formElementId in this.state.controls) {
			formData[formElementId] = this.state.controls[formElementId].value
		}

		this.props.navigateToNextPage(formData);
	}

	render() {

		return (

			<View style={styles.questionContainer}>

				<View style={styles.questionFormContainer}>
					<View>
						<CustomInput placeholder={this.state.controls.address.placeholderText}
							value={this.state.controls.address.value}
							onChangeText={(value) => this.updateInputChange('address', value, '')}
							valid={this.state.controls.address.valid}
							touched={this.state.controls.address.touched}
						/>
					</View>
					<View>
						<CustomInput placeholder={this.state.controls.city.placeholderText}
							value={this.state.controls.city.value}
							onChangeText={(value) => this.updateInputChange('city', value, '')}
							valid={this.state.controls.city.valid}
							touched={this.state.controls.city.touched}
						/>
					</View>
					<View>
						<CustomPicker options={this.state.controls.state.options}
							selectedValue={this.state.controls.state.placeholderText}
							onValueChange={value => this.selectFormHandler(value)}
							valid={this.state.controls.state.valid}
							touched={this.state.controls.state.touched}
						/>
					</View>
				</View>

				<View style={styles.buttonContainer}>
					<Button
						onPress={this.submitFormAndNavigate}
						disabled={!this.state.formIsValid}
						> 
							Continue
					</Button>
				</View>

			</View>

		);
	}

}


export default AddressForm;