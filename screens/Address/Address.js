import React, { Component } from 'react';
import {
	View,
	ScrollView,
	KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';

import styles from './styles';
import AddressForm from './AddressForm';
import Header from '../../components/Header';

import * as actionCreators from '../../store/actions';

class Address extends Component {


	constructor() {
		super();

		this.state = {
			showForm: false
		}
	}

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}

	/**
	 * Go back to previous page
	 */
	goBack = () => {
		this.props.navigator.push({
		  screen: 'Borrow.Name',
		  title: 'Name',
		  passProps: {
			  firstname: this.props.firstname,
			  lastname: this.props.lastname,
			  gender:this.props.gender 
		  }
		});
	}


	navigateToNextPage = formData => {

		this.props.storeAddress(formData);

		this.props.navigator.push({
			screen: 'Borrow.Account',
			title: 'Loan',
			passProps: {
				bank_name: this.props.bank_name,
				account_number: this.props.account_number,
				bvn: this.props.bvn, 
				phone_number: this.props.phone_number
			}
		});

	}

	componentDidMount = () => {
		if (this.props.states.length > 0) {
			this.setState({
				showForm: true
			});
		}
	}

	render () {

		let addressFormPlaceholder = null;

		if (this.state.showForm) {
			addressFormPlaceholder = (
				<View style={styles.formContainer}>
					<AddressForm 
						navigateToNextPage={(formData) => this.navigateToNextPage(formData)} 
						states={this.props.states} 
						address={this.props.address}	
						city={this.props.city}	
						state={this.props.state}
					/>
				</View>
			)
		}

		return (
			<KeyboardAvoidingView style={styles.container}>
				<ScrollView contentContainerStyle={styles.container} showsVerticalScrollIndicator={false}>

					<View style={styles.headerContainer}>
						<Header questionTitle='Your Location' 
								questionSubtitle='We need to know where you live' 
								stepNumber='2' 
								progress='22.22%' 
								onPress={this.goBack}
								showBackButton={true}
						/>
					</View>

					{addressFormPlaceholder}
					
				</ScrollView>
			</KeyboardAvoidingView>
		);
	}

}

const mapStateToProps = state => {
	return {
		states: state.authReducer.states,

		firstname: state.userReducer.firstname,
		lastname: state.userReducer.lastname,
		gender: state.userReducer.gender,

		bank_name: state.userReducer.bank_name,
		account_number: state.userReducer.account_number,
		bvn: state.userReducer.bvn,
		phone_number: state.userReducer.phone_number
	}
}

const mapDispatchToProps = dispatch => {
	return {
		storeAddress: (formData) => dispatch(actionCreators.storeAddress(formData))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Address);