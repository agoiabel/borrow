import React, { Component } from 'react';
import {
	View
} from 'react-native';

import styles from './styles';
import validate from '../../../utility/validation';
import Button from '../../../components/UI/Button';
import CustomInput from '../../../components/UI/CustomInput';
import CustomPicker from '../../../components/UI/CustomPicker';
import CustomDatePicker from '../../../components/UI/CustomDatePicker';

class LoanForm extends Component {

    constructor(props) {
        super(props);

		let newAmountArray = [];
		props.amounts.map(amount => {
			newAmountArray.push({
				label:  '₦ ' + amount.amount,
				value: amount.id.toString()
			})
		});
		const loanPurposeArray = [
			{
				label: 'Pay another loan',
				value: 'pay another loan',
			},
			{
				label: 'School fees',
				value: 'School fees',
			},
			{
				label: 'Others',
				value: 'others',
			}
		];


        this.state = {
			formIsValid: (props.loan_amount && props.loan_purpose && props.refund_date && props.monthly_salary) ? true : false,
            controls: {
            	loan_amount: {
					value: (props.loan_amount) ? props.loan_amount : 'Choose loan amount',
					valid: (props.loan_amount) ? true : false,
					touched: (props.loan_amount) ? true : false,
            		validationRules: {
            			isRequired: true
            		},
					placeholderText: (props.loan_amount) ? this.getLabelForm(props.loan_amount, newAmountArray) : 'Choose loan amount',
					options: newAmountArray
            	},
            	loan_purpose: {
					value: (props.loan_purpose) ? props.loan_purpose : 'Choose loan purpose',
					valid: (props.loan_purpose) ? true : false,
					touched: (props.loan_purpose) ? true : false,
            		validationRules: {
            			isRequired: true
            		},
					placeholderText: (props.loan_purpose) ? this.getLabelForm(props.loan_purpose, loanPurposeArray) : 'Choose loan purpose',
            		options: loanPurposeArray
            	},
            	refund_date: {
					value: (props.refund_date) ? props.refund_date : 'Choose refund date',
					valid: (props.refund_date) ? true : false,
					touched: (props.refund_date) ? true : false,
            		validationRules: {
            			isRequired: true
            		},
					placeholderText: (props.refund_date) ? props.refund_date : 'Choose refund date',
				},
				monthly_salary: {
					value: (props.monthly_salary) ? props.monthly_salary : '',
					valid: (props.monthly_salary) ? true : false,
					touched: (props.monthly_salary) ? true : false,
					validationRules: {
						isRequired: true
					},
					placeholderText: (props.monthly_salary) ? props.monthly_salary : 'What is your monthly salary',
				},
            }
        };
	}
	

	getLabelForm = (value, array) => {

		let label = null;

		array.map(option => {
			if (option.value == value) {
				label = option.label;
			}
		});

		return label;
	}

    selectOptionHandler = (option, controller) => {
    	this.updateInputChange(controller, option.value, option.label);
	}
	

	selectDateHandler = (value, controller) => {
		this.updateInputChange(controller, value, '');
	}

	updateInputChange = (key, value, placeholderText) => {
		const updatedControls = {
			...this.state.controls
		};
		const updatedFormElement = {
			...updatedControls[key]
		};
		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.placeholderText = placeholderText;
		updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

		updatedControls[key] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedControls) {
			formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			formIsValid: formIsValid,
			controls: updatedControls
		});
	}

	/**
	 * Handle the login process
	 * 
	 * @return 
	 */
	submitFormAndNavigate = () => {
		const formData = {};
		for (let formElementId in this.state.controls) {
			formData[formElementId] = this.state.controls[formElementId].value
		}

		this.props.navigateToNextPage(formData);	
	}

	render() {
		return (


			<View style={styles.questionContainer}>
			
				<View style={styles.questionFormContainer}>
					<View>
						<CustomPicker options={this.state.controls.loan_amount.options}
							selectedValue={this.state.controls.loan_amount.placeholderText}
							onValueChange={(input) => this.selectOptionHandler(input, 'loan_amount', '')}
							valid={this.state.controls.loan_amount.valid}
							touched={this.state.controls.loan_amount.touched}
						/>
					</View>
					<View>
						<CustomPicker options={this.state.controls.loan_purpose.options}
							selectedValue={this.state.controls.loan_purpose.placeholderText}
							onValueChange={(input) => this.selectOptionHandler(input, 'loan_purpose', '')}
							valid={this.state.controls.loan_purpose.valid}
							touched={this.state.controls.loan_purpose.touched}
						/>
					</View>
					<View>
						<CustomDatePicker
							onValueChange={(input) => this.selectDateHandler(input, 'refund_date')}
							selectedValue={this.state.controls.refund_date.value}
							valid={this.state.controls.refund_date.valid}
							touched={this.state.controls.refund_date.touched}
						/>
					</View>
					<View>
						<CustomInput placeholder='Monthly salary'
							value={this.state.controls.monthly_salary.value}
							onChangeText={(value) => this.updateInputChange('monthly_salary', value, '')}
							valid={this.state.controls.monthly_salary.valid}
							touched={this.state.controls.monthly_salary.touched}
						/>
					</View>
				</View>

				<View style={styles.buttonContainer}>
					<Button
						onPress={this.submitFormAndNavigate}
						disabled={!this.state.formIsValid}
						>
						Continue
					</Button>
				</View>
			
			</View>

		);
	}

}

export default LoanForm;