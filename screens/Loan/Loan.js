import React, { Component } from 'react';
import {
	View,
	ScrollView,
	KeyboardAvoidingView
} from 'react-native';

import { connect } from 'react-redux';

import styles from './styles';
import LoanForm from './LoanForm';
import Header from '../../components/Header';

import * as actionCreators from '../../store/actions';

class Loan extends Component {

	constructor() {
		super();

		this.state = {
			showForm: false
		}
	}
	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}


	componentDidMount = () => {
		if (this.props.amounts.length > 0) {
			this.setState({
				showForm: true
			});
		}
	}

	
	/**
	 * Handle the login process
	 * 
	 * @return 
	 */
	navigateToNextPage = formData => {
		this.props.storeLoan(formData);

		this.props.navigator.push({
			screen: 'Borrow.Guarantor',
			title: 'Guarantor',
			passProps: {
				guarantor_name: this.props.guarantor_name,
				guarantor_relationship: this.props.guarantor_relationship,
				guarantor_phone_number: this.props.guarantor_phone_number,
				employer_phone_number: this.props.employer_phone_number,
			}
		});
	}

	goBack = () => {
		this.props.navigator.push({
		  screen: 'Borrow.Account',
		  title: 'Account',
		  passProps: {
			  bank_name: this.props.bank_name,
			  account_number: this.props.account_number,
			  bvn: this.props.bvn,
			  phone_number: this.props.phone_number,
		  }
		});
	}

	render () {

		let loanFormPlaceholder = null;

		if (this.state.showForm) {
			loanFormPlaceholder = (
				<View style={styles.formContainer}>
					<LoanForm navigateToNextPage={(formData) => this.navigateToNextPage(formData)} 
							  amounts={this.props.amounts} 
							  loan_amount={this.props.loan_amount}							  
							  loan_purpose={this.props.loan_purpose}							  
							  refund_date={this.props.refund_date}							  
							  monthly_salary={this.props.monthly_salary}							  
					/>
				</View>
			)
		}


		return (
			<KeyboardAvoidingView style={styles.container}>
				<ScrollView contentContainerStyle={styles.container} showsVerticalScrollIndicator={false}>

					<View style={styles.headerContainer}>
						<Header questionTitle='Loan Details' 
								questionSubtitle='Amount to borrow and refund date' 
								stepNumber='4' 
								progress='44.44%' 
								onPress={this.goBack}
								showBackButton={true}
						/>
					</View>

					{ loanFormPlaceholder }

				</ScrollView>
			</KeyboardAvoidingView>
		);
	}
}

const mapStateToProps = state => {
	return {
		amounts: state.authReducer.amounts,

		bank_name: state.userReducer.bank_name,
		account_number: state.userReducer.account_number,
		bvn: state.userReducer.bvn,
		phone_number: state.userReducer.phone_number,

		guarantor_name: state.userReducer.guarantor_name,
		guarantor_relationship: state.userReducer.guarantor_relationship,
		guarantor_phone_number: state.userReducer.guarantor_phone_number,
		employer_phone_number: state.userReducer.employer_phone_number,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		storeLoan: (formData) => dispatch(actionCreators.storeLoan(formData))
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(Loan);