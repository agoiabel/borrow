import React, { Component } from 'react';
import {
	View,
	ScrollView,
	KeyboardAvoidingView
} from 'react-native';

import { connect } from 'react-redux';

import styles from './styles';
import AccountForm from './AccountForm';
import Header from '../../components/Header';

import * as actionCreators from '../../store/actions';

class Account extends Component {

	constructor() {
		super();

		this.state = {
			showForm: false
		}
	}

	componentDidMount = () => {
		if (this.props.banks.length > 0) {
			this.setState({
				showForm: true
			});
		}
	}

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}

	/**
	 * Handle the login process
	 * 
	 * @return 
	 */
	navigateToNextPage = formData => {
		this.props.storeAccount(formData);

		this.props.navigator.push({
			screen: 'Borrow.Loan',
			title: 'Loan',
			passProps: {
				loan_amount: this.props.loan_amount,
				loan_purpose: this.props.loan_purpose,
				refund_date: this.props.refund_date,
				monthly_salary: this.props.monthly_salary
			}
		});

	}

	goBack = () => {
		this.props.navigator.push({
		  screen: 'Borrow.Address',
		  title: 'Address',
		  passProps: {
			  address: this.props.address,
			  city: this.props.city,
			  state: this.props.state 
		  }
		});
	}

	render () {
		let accountFormPlaceholder = null;

		if (this.state.showForm) {
			accountFormPlaceholder = (
				<View style={styles.formContainer}>
					<AccountForm 
						navigateToNextPage={(formData) => this.navigateToNextPage(formData)} 
						banks={this.props.banks} 	
						bank_name={this.props.bank_name}
						account_number={this.props.account_number}
						bvn={this.props.bvn}
						phone_number={this.props.phone_number}
					/>
				</View>
			)
		}

		return (
			<KeyboardAvoidingView style={styles.container}>
				<ScrollView contentContainerStyle={styles.container} showsVerticalScrollIndicator={false}>

					<View style={styles.headerContainer}>
						<Header questionTitle='Account Details' 
								questionSubtitle='The phone number must be connected to your bvn' 
								stepNumber='3' 
								progress='33.33%' 
								onPress={this.goBack}
								showBackButton={true}
						/>
					</View>

					{accountFormPlaceholder}

				</ScrollView>
			</KeyboardAvoidingView>		
		);
	}

}

const mapStateToProps = state => {
	return {
		banks: state.authReducer.banks,

		address: state.userReducer.address,
		city: state.userReducer.city,
		state: state.userReducer.state,

		loan_amount: state.userReducer.loan_amount,
		loan_purpose: state.userReducer.loan_purpose,
		refund_date: state.userReducer.refund_date,
		monthly_salary: state.userReducer.monthly_salary
	}
}

const mapDispatchToProps = dispatch => {
	return {
		storeAccount: (formData) => dispatch(actionCreators.storeAccount(formData))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Account);