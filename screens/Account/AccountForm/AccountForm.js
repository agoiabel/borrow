import React, { Component } from 'react';
import {
	View
} from 'react-native';

import styles from './styles';
import Button from '../../../components/UI/Button';
import CustomInput from '../../../components/UI/CustomInput';
import CustomPicker from '../../../components/UI/CustomPicker';
import validate from '../../../utility/validation';

class AccountForm extends Component {

    constructor(props) {
		super(props);
		
		let newBankArray = [];
		props.banks.map(bank => {
			newBankArray.push({
				label: bank.name,
				value: bank.code.toString()
			})
		});

        this.state = {
			formIsValid: (props.bank_name && props.account_number && props.bvn && props.phone_number) ? true : false,
            controls: {
            	bank_name: {
					value: (props.bank_name) ? props.bank_name : 'Select your bank',
					valid: (props.bank_name) ? true : false,
					touched: (props.bank_name) ? true : false,
            		validationRules: {
            			isRequired: true
            		},
					placeholderText: (props.bank_name) ? this.getLabelForm(props.bank_name, newBankArray) : 'Select your bank',
            		options: newBankArray
            	},
            	account_number: {
					value: (props.account_number) ? props.account_number : '',
					valid: (props.account_number) ? true : false,
					touched: (props.account_number) ? true : false,
            		validationRules: {
						isRequired: true,
						minLength: 10,
						maxLength: 10
					},
					placeholderText: (props.account_number) ? props.account_number : 'account number'
            	},
            	bvn: {
            		value: (props.bvn) ? props.bvn : '',
					valid: (props.bvn) ? true : false,
					touched: (props.bvn) ? true : false,
            		validationRules: {
						isRequired: true,
						minLength: 11,
						maxLength: 11
					},
					placeholderText: (props.bvn) ? props.bvn : 'BVN'
            	},
            	phone_number: {
					value: (props.phone_number) ? props.phone_number : '',
					valid: (props.phone_number) ? true : false,
					touched: (props.phone_number) ? true : false,
            		validationRules: {
						isRequired: true,
						minLength: 11,
						maxLength: 11
					},
					placeholderText: (props.phone_number) ? props.phone_number : 'Phone number'
            	}
            }
        };

	}
	
	getLabelForm = (value, array) => {

		let label = null;

		array.map(option => {
			if (option.value == value) {
				label = option.label;
			}
		});

		return label;
	}

    selectFormHandler = option => {
		this.updateInputChange('bank_name', option.value, option.label);
    }

	updateInputChange = (key, value, placeholderText) => {
		const updatedControls = {
			...this.state.controls
		};
		const updatedFormElement = {
			...updatedControls[key]
		};
		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.placeholderText = placeholderText;
		updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

		updatedControls[key] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedControls) {
			formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			formIsValid: formIsValid,
			controls: updatedControls
		});
	}

	/**
	 * Handle the login process
	 * 
	 * @return 
	 */
	submitFormAndNavigate = () => {

		const formData = {};
		for (let formElementId in this.state.controls) {
			formData[formElementId] = this.state.controls[formElementId].value
		}

		this.props.navigateToNextPage(formData);
	}

	render() {

		/**
		 * Handle the login process
		 * 
		 * @return 
		 */
		submitFormAndNavigate = () => {
			const formData = {};
			for (let formElementId in this.state.controls) {
				formData[formElementId] = this.state.controls[formElementId].value
			}

			this.props.navigateToNextPage(formData);
		}

		return (

			<View style={styles.questionContainer}>
			
				<View style={styles.questionFormContainer}>
					<View>
						<CustomPicker options={this.state.controls.bank_name.options}
							selectedValue={this.state.controls.bank_name.placeholderText}
							onValueChange={option => this.selectFormHandler(option)}
							valid={this.state.controls.bank_name.valid}
							touched={this.state.controls.bank_name.touched}
						/>
					</View>

					<View>
						<CustomInput placeholder={this.state.controls.account_number.placeholderText}
							value={this.state.controls.account_number.value}
							onChangeText={(value) => this.updateInputChange('account_number', value, '')}
							valid={this.state.controls.account_number.valid}
							touched={this.state.controls.account_number.touched}
							keyboardType='numeric'
						/>
					</View>

					<View>
						<CustomInput placeholder={this.state.controls.bvn.placeholderText}
							value={this.state.controls.bvn.value}
							onChangeText={(value) => this.updateInputChange('bvn', value, '')}
							valid={this.state.controls.bvn.valid}
							touched={this.state.controls.bvn.touched}
						 keyboardType='numeric'
						/>
					</View>

					<View>
						<CustomInput placeholder={this.state.controls.phone_number.placeholderText}
							value={this.state.controls.phone_number.value}
							onChangeText={(value) => this.updateInputChange('phone_number', value, '')}
							valid={this.state.controls.phone_number.valid}
							touched={this.state.controls.phone_number.touched}
							keyboardType='numeric'
						/>
					</View>
				</View>

				<View style={styles.buttonContainer}>
					<Button
						onPress={this.submitFormAndNavigate}
						disabled={!this.state.formIsValid}
						>
						Continue
					</Button>
				</View>

			</View>

		);
	}
}

export default AccountForm;