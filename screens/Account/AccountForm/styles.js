import {
	StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
	questionContainer: {
		marginLeft: 'auto',
		marginRight: 'auto',
		width: '90%',
		flex: 1,
	},
	questionFormContainer: {
		marginBottom: 'auto'
	},
	buttonContainer: {}
});

export default styles;