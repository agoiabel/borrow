import React, { Component } from 'react';
import {
	View,
	Text,
	Alert,
	Platform,
	Dimensions
} from 'react-native';


import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions';

import styles from './styles';
import Header from '../../components/Header';
import Button from '../../components/UI/Button';

class SubmitLoan extends Component {

    constructor(props) {
        super(props);

        this.state = {
			viewMode: Dimensions.get('window').height > 500 ? 'portrait' : 'landscape',
			total_refund: 0,
			loan_amount: 0,
			showLoading: false
        };
    }

	getTotalRefund = () => {
		let amountObject = {};

		this.props.amounts.map(amount => {
			if (amount.id == this.props.amount_to_borrow) {
				amountObject = amount;
			}
		});

		this.setState({
			loan_amount: amountObject.amount
		});

		let date1 = new Date();
		let date2 = new Date(this.props.refund_date);
		let diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));

		const percentage_per_day = (parseFloat(amountObject.percentage) / 100) / 30;
		const percentage_for_all_days = percentage_per_day * diffDays;
		const percentage_on_loan = percentage_for_all_days * amountObject.amount;
		return parseInt(percentage_on_loan) + parseInt(amountObject.amount);
	}

	componentDidMount() {
		this.setState({
			total_refund: this.getTotalRefund()
		});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.request_status || nextProps.request_status === 0) {
			this.setState({
				showLoading: false
			});
			Alert.alert(nextProps.request_message);
		}
	}

	processMyLoan = () => {

		this.setState({
			showLoading: true
		});

		if (Platform.OS === 'ios') {
			if (!this.props.user.contact || this.props.user.contact === 0) {
				return alert('Please click accept loan button again');
			}			
		}

		if (Platform.OS === 'android') {
			if (!this.props.user.call_log || this.props.user.call_log === 0 || !this.props.user.sms || this.props.user.sms === 0 || !this.props.user.contact || this.props.user.contact === 0) {
				return alert('Please click process my loan button again');
			}
		}

		this.props.submitLoanRequest(this.props.user);
	}


	goBack = () => {
		this.props.navigator.push({
		  screen: 'Borrow.Identity',
		  title: 'Identity'
		});
	}

	/**
	 * Remove navigator
	 * 
	 * @type 
	 */
	static navigatorStyle = {
		navBarHidden: true
	}

	render () {

		return (
			<View style={styles.TermContainer}>

				<View style={styles.headerContainer}>
					<Header questionTitle='Terms' 
							questionSubtitle='Please take your time to read our terms' 
							stepNumber='9' 
							progress='100%' 
							onPress={this.goBack}
							showBackButton={true}
					/>
				</View>

				<View style={styles.bodyContainer}>

					<View style={this.state.viewMode === 'portrait' ? styles.portraitTermInstructionContainer : styles.landscapeTermInstructionContainer}>

						<View style={this.state.viewMode === 'portrait' ? '' : styles.landscapeTermContainer}>
							<View style={this.state.viewMode === 'portrait' ? styles.loanPrice : styles.landscapeLoanPrice} elevation={5}>
								
								<View style={styles.loanAmount}>
									<Text style={styles.loanHeader}>Loan amount</Text>
									<Text style={styles.loanBody}>&#8358; {this.state.loan_amount}</Text>
								</View>

								<View style={this.state.viewMode === 'portrait' ? styles.verticalLine : ''}></View>
								
								<View style={styles.totalRefund}>
									<Text style={styles.loanHeader}>Total refund</Text>
									<Text style={styles.loanBody}>
										&#8358; {this.state.total_refund}
									</Text>
								</View>

								<View style={this.state.viewMode === 'portrait' ? styles.verticalLine : ''}></View>

								<View style={styles.refundDate}>
									<Text style={styles.loanHeader}>Refund date</Text>
									<Text style={styles.loanBody}>{this.props.refund_date}</Text>
								</View>
							</View>
						</View>

					</View>

					<View style={styles.footerContainer}>
						<Button 
							onPress={this.processMyLoan}
							isLoading={this.state.showLoading}
							disabled={this.state.showLoading}
							> 
								Process my loan
						</Button>
					</View>

				</View>
			</View>
		);
	}

}

const mapStateToProps = state => {
	return {
		amounts: state.authReducer.amounts,
		amount_to_borrow: state.userReducer.loan_amount,
		refund_date: state.userReducer.refund_date,

		user: state.userReducer,
		request_status: state.userReducer.request_status,
		request_message: state.userReducer.request_message,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		submitLoanRequest: (userObject) => dispatch(actionCreators.submitLoanRequest(userObject)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmitLoan);