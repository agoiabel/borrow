package com.borrow;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

import com.BV.LinearGradient.LinearGradientPackage; 
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactnativenavigation.NavigationApplication;
import com.imagepicker.ImagePickerPackage;
import com.wscodelabs.callLogs.CallLogPackage;
import ca.bigdata.voice.contacts.BDVSimpleContactsPackage;
import com.rhaker.reactnativesmsandroid.RNSmsAndroidPackage;

public class MainApplication extends NavigationApplication {


     @Override
     public boolean isDebug() {
         // Make sure you are using BuildConfig from your own application
         return BuildConfig.DEBUG;
     }

     protected List<ReactPackage> getPackages() {
         // Add additional packages you require here
         // No need to add RnnPackage and MainReactPackage
         return Arrays.<ReactPackage>asList(
              new LinearGradientPackage(),
              new VectorIconsPackage(),
              new ImagePickerPackage(),
              new CallLogPackage(),
              new BDVSimpleContactsPackage(),
              new RNSmsAndroidPackage()
         );
     }

     @Override
     public List<ReactPackage> createAdditionalReactPackages() {
         return getPackages();
     }

}
