import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	Dimensions,
	TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
// import LinearGradient from 'react-native-linear-gradient';
import CustomHeaderText from '../../components/UI/CustomHeaderText';

class Header extends Component {

	constructor () {
		super();
		this.state = {
			viewMode: Dimensions.get('window').height > 500 ? 'portrait' : 'landscape'
		};
		Dimensions.addEventListener('change', this.updateViewMode);
	}

	/**
	 * Handle the process of updating view mode base on height
	 * 
	 * @param  dimensions 
	 * @return 
	 */
	updateViewMode = dimensions => {
		this.setState({
			viewMode: dimensions.window.height > 500 ? 'portrait' : 'landscape'
		});
	}

	/**
	 * Make sure i unsubscribe from the listener
	 * 
	 * @return 
	 */
	componentWillUnmount () {
		Dimensions.removeEventListener('change', this.updateViewMode);	
	}


	render () {

		let backButton;

		if (this.props.showBackButton) {
			backButton = (
				<TouchableOpacity onPress={this.props.onPress} style={styles.goBack}>
					<Icon name="ios-arrow-round-back" size={26} color="#FFFFFF" /> 
					<Text style={styles.back}>Back</Text>
				</TouchableOpacity>
			);
		}

		return (

			// <LinearGradient 
			// 	colors={['#6A60EE', '#56EDFF']} 
			// 	style={styles.linearGradient}
			// 	start={{x: 0.5, y: 1.0}}
			// 	end={{x: 1.0, y: 1.0}}
			// 	locations={[0, 0.9]}
			// 	>
				<View style={this.state.viewMode === 'portrait' ? styles.portraitHeaderContainer : styles.landscapeHeaderContainer}>

						{ backButton }
						
						<View style={styles.question}>
							<View style={styles.questionTitle}>
								<CustomHeaderText fontSize='18' color='#FFFFFF'> {this.props.questionTitle} </CustomHeaderText>
							</View>		
							<View style={styles.questionSubtitle}>
								<CustomHeaderText fontSize='13' color='#FFFFFF'> {this.props.questionSubtitle} </CustomHeaderText>
							</View>			
						</View>

						<View style={styles.questionProgress}>
							<View style={styles.questionProgressTitle}>
								<Text style={styles.mainText}>Questions</Text>
								<Text style={styles.mainText}>{this.props.stepNumber} of 9</Text>
							</View>

							<View style={styles.questionProgressStatus}>
								<View style={[styles.questionProgressActive, {width: this.props.progress}]}></View>
							</View>
						</View>

				</View>
			// </LinearGradient>

		);
	}

}

const styles = StyleSheet.create({
	goBack: {
		width: '90%',
		marginLeft: 'auto',
		marginRight: 'auto',

		flexDirection: 'row',
	},
	back: {
		marginTop: 5,
		marginLeft: 5,
		color: '#FFFFFF'
	},
	portraitHeaderContainer: {
		paddingTop: 45,
		paddingBottom: 25,
		width: '100%',
		backgroundColor: '#19B4E5'
		// flex: 1
	},
	landscapeHeaderContainer: {
		paddingTop: 10,
		paddingBottom: 8,
		width: '100%',
		backgroundColor: '#19B4E5'
		// flex: 1
	},
	question: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	questionTitle: {
		marginBottom: 10
	},
	questionSubtitle: {
		marginBottom: 10
	},
	questionProgress: {
		width: '90%',
		marginLeft: 'auto',
		marginRight: 'auto',
	},
	questionProgressTitle: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 10
	},
	mainText: {
		color: '#FFFFFF'
	},
	questionProgressStatus: {
      backgroundColor: '#FFFFFF',
      borderRadius: 6, 
      padding: 3
	},
	questionProgressActive: {
       backgroundColor: '#50E3C2',
       height: 6,
       borderRadius: 10
	}
});

export default Header;