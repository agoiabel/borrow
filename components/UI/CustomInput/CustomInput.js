import React from 'react';
import {
	View,
	Text,
	StyleSheet,
	TextInput
} from 'react-native';

const CustomInput = props => {

	const inputClasses = [styles.formControl];
	

	return (
		<TextInput  
			style={[
				styles.formControl,
				!props.valid && props.touched ? styles.inValid : null
			]}
			underlineColorAndroid='transparent'
			{...props}
		/>
	);
}

const styles = StyleSheet.create({
	formControl: {
		width: '100%',
		borderWidth: 1,
		borderColor: '#CBCBCB',
		padding: 12,
		borderRadius: 5,
		marginBottom: 15
	},
	inValid: {
		borderColor: '#C51162',
	},

});

export default CustomInput;