import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Text,
	Modal,
	ActivityIndicator
} from 'react-native';


class ModalLoader extends Component {
 
	constructor (props) {
		super(props);

		this.state = {
			modalVisibility: props.showLoading
		};
	}

	setModalVisible(visible) {
		this.setState({modalVisible: visible});
	}

  render() {
	    return (
			<Modal
				animationType="slide"
				transparent={true}
				visible={true}
				onRequestClose={() => {
					// console.dir('Modal has been closed');
				}}
				>
				<View style={styles.modalContainer}>

					<View style={styles.modalContent}>
						
						<ActivityIndicator size="large" color="#9C9C9C" />
						<Text style={styles.loadingText}>{ this.props.message }</Text>

					</View>

				</View>
			</Modal>
	    );
	}
}

const styles = StyleSheet.create({
	modalContainer: {
		marginTop: 30,
		backgroundColor: 'rgba(0,0,0,0.4)',
		flex: 1
	},
	modalContent: {
		width: '80%',
		maxHeight: '90%',
		marginLeft: 'auto',
		marginRight: 'auto',
		marginTop: 'auto',
		marginBottom: 'auto',
		backgroundColor: '#FFFFFF',

		padding: 35,
		borderRadius: 5,
		flexDirection: 'row' 
	},
	loadingText: {
		marginTop: 10,
		marginLeft: 15
	}
});

export default ModalLoader;