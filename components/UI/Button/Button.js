import React from 'react';
import {
	Text,
	View,
	StyleSheet,
	TouchableOpacity,
	ActivityIndicator
} from 'react-native';

const Button = props => {

	const disabled = props.disabled;
	const isLoading = props.isLoading;

	let content = (
		<View style={disabled ? styles.inValidButton : styles.validButton}>
			<Text style={styles.buttonText}> {props.children} </Text>
		</View>
	);

	if (isLoading) {
		content = (
			<View style={disabled ? styles.inValidButton : styles.validButton}>
				<View style={styles.buttonContent}>
					<View style={styles.activityLoader}>
						<ActivityIndicator size="large" color="white" />
					</View>
					<Text style={styles.loadingText}> Please wait.. </Text>
				</View>
			</View>
		);
	}

	if (disabled || isLoading) {
		return content;
	}

	return (
		<TouchableOpacity onPress={props.onPress}>
			{ content }
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	validButton: {
		width: '100%',
		borderRadius: 5,
		backgroundColor: '#19B4E5',
		padding: 10,
		height: 50,
		justifyContent: 'center',
		alignItems: 'center',
	},
	inValidButton: {
		width: '100%',
		borderRadius: 5,
		backgroundColor: 'rgba(25, 180, 229, 0.6)',
		padding: 10,
		height: 50,
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonText: {
		color: 'white',
		fontSize: 16
	},
	loadingText: {
		color: 'white',
		fontSize: 16
	},
	buttonContent: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
	},
	activityLoader: {
		marginRight: 15
	}
});

export default Button;