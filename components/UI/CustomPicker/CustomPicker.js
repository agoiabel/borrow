import React, { Component } from 'react';
import {
	Picker,
	StyleSheet,
	View,
	Text,
	Platform,
	Modal,
	ScrollView,
	TouchableOpacity,
	TouchableHighlight
} from 'react-native';

class CustomPicker extends Component {

	constructor() {
		super();

		this.state = {
			modalVisibility: false
		};
	}

	onValueChange = option => {
		// console.dir(option);
	}

	setModalVisibilityTo = visibility => {
		this.setState({
			modalVisibility: visibility
		});
	}

	selectedOptionInModal = option => {
		this.props.onValueChange(option);
		this.setModalVisibilityTo(!this.state.modalVisibility);
	}

	render() {


		const options = this.props.options.map((optionObject, index) => (
			<TouchableHighlight style={styles.option}
				onPress={(event) => this.selectedOptionInModal(optionObject)}
				key={index}>
				<Text>{optionObject.label}</Text>
			</TouchableHighlight>
		))


		return (

			<View>
				<TouchableOpacity onPress={() => {
					this.setModalVisibilityTo(true)
				}}>

					<Text style={styles.formControl}>{this.props.selectedValue}</Text>
				</TouchableOpacity>

				<Modal
					animationType="slide"
					transparent={true}
					visible={this.state.modalVisibility}
					onRequestClose={() => {
						// console.dir('Modal has been closed');
					}}
				>
					<View style={styles.modalContainer}>

						<View style={styles.modalContent}>
							<ScrollView style={styles.options}>{options}</ScrollView>
						</View>

					</View>
				</Modal>

			</View>
		)

	}
}

const styles = StyleSheet.create({
	picker: {
		height: 50,
		width: '100%',
	},
	modalContainer: {
		marginTop: 30,
		backgroundColor: 'rgba(0,0,0,0.4)',
		flex: 1
	},
	modalContent: {
		width: '95%',
		maxHeight: '90%',
		marginLeft: 'auto',
		marginRight: 'auto',
		marginTop: 'auto',
		marginBottom: 'auto',
		backgroundColor: '#FFFFFF',

		borderRadius: 5
	},
	options: {},
	option: {
		paddingLeft: 5,
		paddingRight: 5,
		paddingBottom: 15,
		paddingTop: 15,
		borderBottomWidth: 1,
		borderStyle: 'solid',
		borderBottomColor: '#CBCBCB',
	},
	formControl: {
		width: '100%',
		borderWidth: 1,
		borderColor: '#CBCBCB',
		padding: 12,
		borderRadius: 5,
		marginBottom: 15,
	},
	androidFormControl: {
		width: '100%',
		borderWidth: 1,
		borderColor: '#CBCBCB',
		padding: 2,
		borderRadius: 5,
		marginBottom: 15,
	}
});

export default CustomPicker;