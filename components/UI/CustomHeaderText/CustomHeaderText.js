import React from 'react';
import {
	Text,
	StyleSheet
} from 'react-native';

const CustomHeaderText = props => {
	let size = parseInt(props.fontSize);

	return (
		<Text style={ [styles.textHeader, {fontSize: size, color: props.color}] }>
			{props.children}
		</Text>
	);
}

const styles = StyleSheet.create({
    textHeader: {
        fontWeight: 'bold'
    }
});

export default CustomHeaderText;