import React, { Component } from 'react';
import { 
	View, 
	Text, 
	Platform, 
	StyleSheet, 
	TouchableOpacity, 
	YellowBox
} from 'react-native';

import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';


class CustomDatePicker extends Component {

	constructor() {
		super();

		this.state = {
			isDatePickerVisible: false
		}
		console.disableYellowBox = true;
	}

	showPickerHandler = () => {
		this.setState({
			isDatePickerVisible: true
		});
	}

	hideDateHandler = () => {
		this.setState({
			isDatePickerVisible: false
		});
	}

	datePickedHandler = date => {
		const maximumDate = new Date().setMonth(new Date().getMonth() + 1);

		if (date > maximumDate) {
			date = maximumDate;
		}

		this.props.onValueChange(moment(date).format('DD-MMM-YYYY'));

		this.hideDateHandler();
	};


	render() {
		return (

			<View>
				<TouchableOpacity onPress={this.showPickerHandler}>
					<Text style={styles.formControl}>{this.props.selectedValue}</Text>
				</TouchableOpacity>

				<DateTimePicker
					isVisible={this.state.isDatePickerVisible}
					onConfirm={this.datePickedHandler}
					onCancel={this.hideDateHandler}
					minimumDate={new Date()}
					maximumDate={Platform.OS === 'ios' ? '' : new Date().setMonth(new Date().getMonth() + 1)}
				/>
			</View>


		);
	}

}

const styles = StyleSheet.create({
	formControl: {
		width: '100%',
		borderWidth: 1,
		borderColor: '#CBCBCB',
		padding: 12,
		borderRadius: 5,
		marginBottom: 15,
	},
});

export default CustomDatePicker;