import React from 'react';

import {
	View,
	Text,
	StyleSheet,
	ScrollView
} from 'react-native';

import CustomHeaderText from '../UI/CustomHeaderText';
import Transaction from './Transaction';

const TransactionContainer = props => {
	return (
		<View style={styles.transactionContainer}>
			<View style={styles.transactionContainerHeader}>
				<CustomHeaderText fontSize='15' color='#787878'>Recent Transactions</CustomHeaderText>
			</View>

			<ScrollView>
				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />


				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />

				<Transaction iconName="ios-checkmark" iconColor="#5BC7FB" />

				<Transaction iconName="ios-close" iconColor="#FCE1B4" />
			</ScrollView>

		</View>
	);
}

const styles = StyleSheet.create({
	transactionContainer: {
		marginTop: 25
	},
	transactionContainerHeader: {
		paddingBottom: 15,
		borderBottomWidth: 1,
		borderStyle: 'solid',
		borderBottomColor: '#EEEEEE'
	},
	transaction: {
		flexDirection: 'row',

		paddingTop: 5,
		paddingBottom: 5,
		borderBottomWidth: 1,
		borderStyle: 'solid',
		borderBottomColor: '#EEEEEE'
	},
	transactionIcon: {
		marginRight: 10
	},
	transactionAmount: {
		marginRight: 'auto',
		paddingTop: 5
	},
	transactionRefundDate: {
		paddingTop: 5
	},
	transactionInfo: {
		color: '#9C9C9C'
	},
	iconStyle: {
		borderRadius: 4,
		borderWidth: 2,
		borderColor: '#5BC7FB',
		paddingRight: 10,
		paddingLeft: 10
	},
	transactionHeader: {
		color: '#7F7F7F',
		fontWeight: 'bold'
	}
});

export default TransactionContainer;