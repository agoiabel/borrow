import React from 'react';
import {
	View,
	Text,
	StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const Transaction = props => {

	return (
		<View style={styles.transaction}>
			<View style={styles.transactionIcon}>
				<Text style={[styles.iconStyle, {borderColor: props.iconColor}]}>
					<Icon name={props.iconName} size={35} color={props.iconColor} />
				</Text>
			</View>
			<View style={styles.transactionAmount}>
				<Text style={styles.transactionHeader}>Loan amount</Text>
				<Text style={styles.transactionInfo}>#20, 000</Text>
			</View>
			<View style={styles.transactionRefundDate}>
				<Text style={styles.transactionHeader}>Date refunded</Text>
				<Text style={styles.transactionInfo}>Dec 3rd, 2018</Text>
			</View>
		</View>
	);

}

const styles = StyleSheet.create({
	transaction: {
		flexDirection: 'row',

		paddingTop: 5,
		paddingBottom: 5,
		borderBottomWidth: 1,
		borderStyle: 'solid',
		borderBottomColor: '#EEEEEE'
	},
	transactionIcon: {
		marginRight: 10
	},
	transactionAmount: {
		marginRight: 'auto',
		paddingTop: 5
	},
	transactionRefundDate: {
		paddingTop: 5
	},
	transactionInfo: {
		color: '#9C9C9C'
	},
	iconStyle: {
		borderRadius: 4,
		borderWidth: 2,
		paddingRight: 10,
		paddingLeft: 10
	},
	transactionHeader: {
		color: '#7F7F7F',
		fontWeight: 'bold'
	}
});

export default Transaction;