export const getLabelForm = (value, array) => {

    let label = null;

    array.map(option => {
        if (option.value == value) {
            label = option.label;
        }
    });

    return label;
}

