// const API_URL = 'http://borrowapi.test/api/';
// const API_URL = 'http://45.76.135.197/api/'
import { get } from './storage';

export const post = async (formDate, end_point) => {

    const AuthToken = await get('Borrow:auth_token');
    
    return fetch('http://45.76.135.197/api/' + end_point, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'AuthToken': AuthToken
        },
        body: formDate
    });

}