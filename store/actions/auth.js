import * as type from './type'; 
import { post } from '../../utility/httpClient';
import { set } from '../../utility/storage';

export const clearOldLogin = () => {
	return {
		type: type.CLEAR_OLD_LOGIN
	};
}

export const startLogin = payload => {
	return async dispatch => {
		
		try {
			const response = await post(JSON.stringify({
				email: payload.email,
				password: payload.password
			}), 'auth/store');

			const data = await response.json();

			// console.dir(data);

			if (data.status === 422) {
				return this.setTimeout((() => {
					dispatch(authenticationUnsuccessful(data));
				}));
			}
			
			try {

				await set("Borrow:auth_token", data.user.auth_token);
				await set("Borrow:auth_status", data.user.status);

			} catch (error) {
				//Error saving token
				alert('Error storing in storage');
			}

			//pass the token to redux
			dispatch(storeAuthenticatedUserData(data));

			dispatch(storeAuthenticatedUserToken(data.user.auth_token));

			dispatch(storeStates(data.states));

			dispatch(storeBanks(data.banks));

			dispatch(storeAmounts(data.amounts));

			return dispatch(storeLoanStatus(data.loan_status));

		} catch (error) {
			// console.dir('network error');
		}

	}
};



export const storeAuthenticatedUserData = payload => {
	return {
		type: type.STORE_AUTH_USER_DATA,
		payload: payload
	};		
}

export const storeAuthenticatedUserToken = payload => {
	return {
		type: type.STORE_USER_TOKEN,
		payload: payload
	}
}

export const authenticationUnsuccessful = payload => {
	//pass the error_message to redux store
	return {
		type: type.AUTH_UNSUCCESSFUL,
		payload: payload
	};	
}

export const storeStates = payload => {
	return {
		type: type.STORE_STATE,
		payload: payload
	};
}

export const storeBanks = payload => {
	return {
		type: type.STORE_BANK,
		payload: payload
	}
}

export const storeAmounts = payload => {
	return {
		type: type.STORE_AMOUNT,
		payload: payload
	}
}

export const storeLoanStatus = payload => {
	return {
		type: type.STORE_LOAN_STATUS,
		payload: payload
	}
}

export const showLoading = payload => {
	return {
		type: type.SHOW_LOADING,
		payload: payload
	}	
}