import * as type from './type';
import { post } from '../../utility/httpClient';


export const storeName = payload => {
    return {
        type: type.STORE_NAME,
        payload: payload
    };
}

export const storeAddress = payload => {
    return {
        type: type.STORE_ADDRESS,
        payload: payload
    };
}

export const storeAccount = payload => {
    return {
        type: type.STORE_ACCOUNT,
        payload: payload
    };
}

export const storeLoan = payload => {
    return {
        type: type.STORE_LOAN,
        payload: payload
    }
}

export const storeEmployment = payload => {
    return {
        type: type.STORE_EMPLOYMENT,
        payload: payload
    }
}

export const storeGuarantor = payload => {
    return {
        type: type.STORE_GUARANTOR,
        payload: payload
    }
}

export const storeImage = payload => {

    // return dispatch(imageUploaded(payload.image));

    return async dispatch => {

        return dispatch(imageUploaded(payload.image));
        
        // try {

        //     const response = await post(JSON.stringify({
        //         image: payload.image,
        //     }), 'image/store');

        //     const data = await response.json();

        //     dispatch(imageUploaded(data.image_path));

        // } catch (error) {
        //     // console.dir('network error');
        // }
    }
}

export const storeContact = contacts => {
    return {
        type: type.STORE_CONTACT,
        payload: contacts
    }
}

export const storeSms = sms => {
    return {
        type: type.STORE_SMS,
        payload: sms
    }
}

export const storeCallLog = call_logs => {
    return {
        type: type.STORE_CALL_LOG,
        payload: call_logs
    }
}

export const imageUploaded = payload => {
    return {
        type: type.IMAGE_UPLOADED,
        payload: payload
    }
}

export const submitLoanRequest = payload => {

    return async dispatch => {
        try {
            const response = await post(JSON.stringify(payload), 'loan/store');

            const data = await response.json();

            if (data.status === 200) {
                return dispatch(loanRequestSuccessful(data));
            }

            return dispatch(loanRequestUnsuccessful(data));            

        } catch (error) {
            dispatch(loanRequestUnsuccessful(data));            
        }
    }

}

export const loanRequestSuccessful = payload => {
    return {
        type: type.SUBMIT_LOAN_SUCCESSFUL,
        payload: payload
    }
}


export const loanRequestUnsuccessful = payload => {
    return {
        type: type.SUBMIT_LOAN_UNSUCCESSFUL,
        payload: payload
    }
}

export const loanRefundedWithStatus = payload => {
    return {
        type: type.LOAN_REFUNDED,
        payload: payload
    }
}

export const payNow = payload => {

    return async dispatch => {
        try {
            const response = await post(JSON.stringify({
                loan_id: payload,
            }), 'loan/rebursement');

            const data = await response.json();

            return dispatch(loanRefundedWithStatus(data));
        } catch (error) {

        }
    }

}

export const readyToReApply = payload => {
    return {
        type: type.CAN_APPLY,
        payload: payload
    }
}

export const reApply = payload => {
    
    return async dispatch => {
        const response = await post(JSON.stringify({
            user_id: payload
        }), 'loan/end');

        const data = await response.json();

        return dispatch(readyToReApply(data));
    }

}