import * as type from './type'; 

export const uiStartLoading = () => {
	return {
		type: type.UI_START_LOADING
	};
}

export const uiStopLoading = () => {
	return {
		type: type.UI_STOP_LOADING
	};
}