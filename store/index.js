import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';


import authReducer from './reducers/authReducer';
import userReducer from './reducers/userReducer';
import uiReducer from './reducers/uiReducer';


let composeEnhancers = compose;
if (__DEV__) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}


const rootReducer = combineReducers({
    authReducer: authReducer,
    userReducer: userReducer,
    uiReducer: uiReducer,
});
const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk))
);


export default store;