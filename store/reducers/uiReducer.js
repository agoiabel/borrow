import * as actionTypes from '../actions/type';

const initialState = {
	isLoading: false
}

const uiReducer = (state = initialState, action) => {
	
	switch (action.type) {
		case actionTypes.UI_START_LOADING:
			
			return {
				...state,
				isLoading: state.isLoading = true,
			}
			
		case actionTypes.UI_STOP_LOADING:

			return {
				...state,
				isLoading: state.isLoading = false,
			}

	}

	return state;

}

export default uiReducer;