import * as actionTypes from '../actions/type';

const initialState = {
    auth_token: '',

    firstname: '',
    lastname: '',
    gender: '',

    address: '',
    city: '',
    state: '',

    bank_name: '',
    account_number: '',
    bvn: '',
    phone_number: '',

    loan_amount: '',
    loan_purpose: '',
    refund_date: '',
    
    monthly_salary: '',

    guarantor_name: '',
    guarantor_relationship: '',
    guarantor_phone_number: '',

    company_name: '',
    employer_name: '',
    employer_phone_number: '',
    job_title: '',

    image_path: null,
    
    contact: '',
    call_log: '',
    sms: '',

    request_status: '',
    request_message: '',

    pay_loan_status: '',
    pay_loan_message: '',
    can_apply_request: '',
}

const userReducer = (state = initialState, action) => {

    switch (action.type) {
        

        case actionTypes.STORE_USER_TOKEN: 

            return {
                ...state,
                auth_token: state.auth_token = action.payload
            }

        case actionTypes.STORE_NAME:

            return {
                ...state,
                firstname: state.firstname = action.payload.firstname,
                lastname: state.lastname = action.payload.lastname,
                gender: state.gender = action.payload.gender
            }

        case actionTypes.STORE_ADDRESS:

            return {
                ...state,
                address: state.address = action.payload.address,
                city: state.city = action.payload.city,
                state: state.state = action.payload.state
            }


        case actionTypes.STORE_ACCOUNT:

            return {
                ...state,
                bank_name: state.bank_name = action.payload.bank_name,
                account_number: state.account_number = action.payload.account_number,
                bvn: state.bvn = action.payload.bvn,
                phone_number: state.phone_number = action.payload.phone_number,
            }


        case actionTypes.STORE_LOAN:

            return {
                ...state,
                loan_amount: state.loan_amount = action.payload.loan_amount,
                loan_purpose: state.loan_purpose = action.payload.loan_purpose,
                refund_date: state.refund_date = action.payload.refund_date,
                monthly_salary: state.monthly_salary = action.payload.monthly_salary,
            }


        case actionTypes.STORE_GUARANTOR:

            return {
                ...state,
                guarantor_name: state.guarantor_name = action.payload.guarantor_name,
                guarantor_relationship: state.guarantor_relationship = action.payload.guarantor_relationship,
                guarantor_phone_number: state.guarantor_phone_number = action.payload.guarantor_phone_number,
                employer_phone_number: state.employer_phone_number = action.payload.employer_phone_number,
            }


        case actionTypes.STORE_EMPLOYMENT:
        
            return {
                ...state,
                company_name: state.company_name = action.payload.company_name,
                employer_name: state.employer_name = action.payload.employer_name,
                employer_phone_number: state.employer_phone_number = action.payload.employer_phone_number,
                job_title: state.job_title = action.payload.job_title,
            }

        case actionTypes.IMAGE_UPLOADED:

            return {
                ...state,
                image_path: state.image_path = action.payload
            }

        case actionTypes.STORE_CONTACT:

            return {
                ...state,
                contact: state.contact = action.payload.contact
            }

        case actionTypes.STORE_CALL_LOG:

            return {
                ...state,
                call_log: state.call_log = action.payload.call_log
            }

        case actionTypes.STORE_SMS:

            return {
                ...state,
                contact: state.sms = action.payload.sms
            }

        case actionTypes.SUBMIT_LOAN_SUCCESSFUL:

            return {
                ...state,
                request_status: state.request_status = action.payload.status,
                request_message: state.request_message = action.payload.message
            }

        case actionTypes.SUBMIT_LOAN_UNSUCCESSFUL:

            return {
                ...state,
                request_status: state.request_status = action.payload.status,
                request_message: state.request_message = action.payload.message
            }

        case actionTypes.LOAN_REFUNDED:
            
            return {
                ...state,
                pay_loan_status: state.pay_loan_status = action.payload.status,
                pay_loan_message: state.pay_loan_message = action.payload.message
            }

        case actionTypes.CAN_APPLY:

            return {
                ...state,
                can_apply_request: state.can_apply_request = action.payload.status
            }
    }

    return state;

}

export default userReducer;