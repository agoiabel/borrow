import * as actionTypes from '../actions/type';

const initialState = {
	auth_token: '',
	auth_status: '',
	err_message: '',
	user: {},
	states: {},
	banks: {},
	amounts: {},
	loan_current_due: '',
	loan_due_date: '',
	loan_total_due: '',
	loan_id: ''
}

const authReducer = (state = initialState, action) => {
	
	switch (action.type) {

		case actionTypes.CLEAR_OLD_LOGIN:
			return {
				...state,
				auth_token: state.auth_token = '',
				user: state.user = {},
				auth_status: state.auth_status = '',
			}


		case actionTypes.STORE_AUTH_USER_DATA:
		
			return {
				...state,
				auth_token: state.auth_token = action.payload.user.auth_token,
				user: state.user = action.payload.user,
				auth_status: state.auth_status = action.payload.status,
			}
			
		case actionTypes.STORE_STATE:

			return {
				...state,
				states: state.states = action.payload
			}

		case actionTypes.STORE_BANK:

			return {
				...state,
				banks: state.banks = action.payload
			}

		case actionTypes.STORE_AMOUNT:

			return {
				...state,
				amounts: state.amounts = action.payload
			}

		case actionTypes.STORE_LOAN_STATUS:
			return {
				...state,
				loan_current_due: state.loan_current_due = action.payload.loan_current_due,
				loan_due_date: state.loan_due_date = action.payload.loan_due_date,
				loan_total_due: state.loan_total_due = action.payload.loan_total_due,
				loan_id: state.loan_id = action.payload.loan_id
			}


		case actionTypes.AUTH_UNSUCCESSFUL: 

			return {
				...state,
				auth_status: state.auth_status = action.payload.status,
				err_message: state.err_message = action.payload.error
			}

	}

	return state;

}

export default authReducer;